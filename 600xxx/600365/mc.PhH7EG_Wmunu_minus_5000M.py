# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

decay_mode = "w- > mu- vm~"

mass_slice = runArgs.jobConfig[0].split("_")[-1]

mass_low=mass_slice.split("M")[0]
mass_high=mass_slice.split("M")[1]

include("PowhegControl_W_H7.py")

evgenConfig.nEventsPerJob = 2000
