######################################################################
# SM QCD diphoton with Sherpa (binned in m_gg)
######################################################################
include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "gamma gamma + 0,1 jets. At least two photons with pT > 5 GeV, with mass cut 0<Mgg<100 GeV"
evgenConfig.process = "QCD direct diphoton production"
evgenConfig.keywords = ["SM", "diphoton"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "jcantero@cern.ch" ]
evgenConfig.nEventsPerJob = 5000

genSeq.Sherpa_i.RunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])}

  % tags for process setup
  NJET:=1; QCUT:=10;

  % me generato settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
}(run)

(processes){

  Process 21 21 -> 22 22
  ME_Generator Internal;
  Loop_Generator gg_yy
  Scales VAR{FSF*Abs2(p[2]+p[3])}{RSF*Abs2(p[2]+p[3])}{QSF*Abs2(p[2]+p[3]);
  End process;

  Process 93 93 -> 22 22 93{NJET}
  Order (*,2)
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/Abs2(p[2]+p[3]))
  %Integration_Error 0.03 {3,4};
  End process;
}(processes)

(selector){
  PT 22 5.0 E_CMS
  IsolationCut 22  0.1  2  0.1
  DeltaR 22 22 0.2 1000.0
  Mass 22  22  0.0 100.0
  Rapidity 22 -2.7 2.7
}(selector)
"""

genSeq.Sherpa_i.NCores = 10 



