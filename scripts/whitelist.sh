#!/bin/bash

# Rules for DSID naming
if $dryrun ; then # this is only true when checkWhitelist is called from the commit script
  naming=".*"
else
  naming="[0-9]{3}xxx/[0-9]{6}"
fi

# Function to check if a file is in the whitelist of files that are allowed to be added in a commit
checkWhiteList() {   
    name=$(echo $1)
    if [ -f $1 -a ! -L $1 ] ; then
       # Regular file checks
        if [[ ($name =~ $naming/.*\.py) ||
              ($name =~ $naming/.*\.f) ||
              (($name =~ $naming/.*\.dat) && !($name =~ $naming/.*/.*\.dat)) ||
              ($name =~ $naming/powheg.input) ||
              ($name =~ $naming/log\.generate\.short) ||
              ($name =~ $naming/MadGraphControl/.*\.py) ||
              ($name =~ $naming/PowhegControl/.*\.py) ||
              ($name =~ $naming/Herwig7_i/.*\.py) ||
              ($name =~ $naming/Pythia8_i/.*\.py) ||
              ($name =~ $naming/Sherpa_i/.*\.py) ]] ; then
            return 0 #true
        else
            return 1 #false
        fi
    elif [ -L $1 ] ; then
        # Link checks
        if [[ ($name =~ $naming/mc_.*TeV\..*\.GRID\.tar\.gz) ||
              ($name =~ $naming/.*\.py) ||
              ($name =~ $naming/.*\.f) ||
              (($name =~ $naming/.*\.dat) && !($name =~ $naming/.*/.*\.dat)) ||
              ($name =~ $naming/powheg.input) ||
              ($name =~ $naming/MadGraphControl/.*\.py) ||
              ($name =~ $naming/PowhegControl/.*\.py) ||
              ($name =~ $naming/Herwig7_i/.*\.py) ||
              ($name =~ $naming/Pythia8_i/.*\.py) ||
              ($name =~ $naming/Sherpa_i/.*\.py) ]] ; then
            return 0 #true
        else
            return 1 #false
        fi
    else
        echo "Unknown file/filetype: $name"
        return 1
    fi
}

