from MadGraphControl.MadGraphUtils import *
import subprocess
#from os.path import join as pathjoin 

fcard = open('proc_card_mg5.dat','w')

safefactor=1.1
nevents=runArgs.maxEvents*safefactor
runName='run_01'

#==================================================================================

print "Generate VBF+VHhad events."
processCommand="""
generate p p > h j j  QCD=0 /a  NP=0, h > e+ ve mu- vm~ NP=0 
add process p p > h j j QCD=0 /a NP=0, h > mu+ vm e- ve~ NP=0 
"""




fcard.write("""
import model SMEFTsim_A_U35_MwScheme_UFO-SMlimit_massless"""
+processCommand+"""
output -f""")
fcard.close()


#==================================================================================



# General settings
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#==================================================================================

#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'pdlabel':'nn23lo1',
#           'ickkw'       : ickkw,
           'ptj'         : 20.,
           'ptb'         : 20.,
           'use_syst'    : "False" }
    

#==================================================================================
process_dir = new_process()

#==================================================================================

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)


#==================================================================================

print_cards()

#==================================================================================



generate(run_card_loc='run_card.dat',param_card_loc=None, proc_dir=process_dir,run_name=runName,nevents=nevents,random_seed=runArgs.randomSeed)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)  

#==================================================================================
# Shower 
evgenConfig.description = 'MadGraphSMEFT_VBF'
evgenConfig.contact     = [ 'ana.cueto@cern.ch' ]
evgenConfig.nEventsPerJob = 10000 
evgenConfig.keywords+=['Higgs']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'



include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")



