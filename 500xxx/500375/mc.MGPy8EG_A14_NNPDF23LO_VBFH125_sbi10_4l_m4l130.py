evgenConfig.inputconfcheck="sbi10_4l"
evgenConfig.nEventsPerJob = 2000

proc_name="VBF4l_SBI10"
m4lmin="130"
m4lmax=None

include("MadGraphControl_Pythia8EvtGen_lllljj_EW6.py")
