#--------------------------------------------------------------
# on-the-fly generation of H+ MG5 events, montoya@cern.ch
#--------------------------------------------------------------
from MadGraphControl.MadGraphUtils import *
import fileinput
import math
import os
import sys


nevents=1.1*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
mode=0
runName='run_01'  
gridpack_dir  = 'madevent/'
gridpack_mode = False

lhaid=260000
pdfmin=260001
pdfmax=260100

reweight_scale = '.true.'
reweight_PDF   = '.true.'
pdflabel='lhapdf'
maxjetflavor=5
parton_shower='PYTHIA8'
muR_over_ref  = 1.0
muF1_over_ref = 1.0
muF2_over_ref = 1.0
dyn_scale = '10'    # user-defined scale -> Dominic's definition of mt+1/2*(pt^2+ptx^2)
lhe_version=3
bwcut = 50

 
processes =["tWmcb", "tWpcb"]
proc = 'generate p p > t t~ [QCD]'
wdecay = ""


if process in processes:
   if process=="tWmcb":
      wdecay = "decay t > w+ b, w+ > lep lep \ndecay t~ > w- b~, w- > c~ b \n"
   elif process=="tWpcb":
      wdecay = "decay t > w+ b, w+ > c b~ \ndecay t~ > w- b~, w- > lep lep \n"
else:
    raise RuntimeError("process not found")
	
# --------------------------------------------------------------
#  Proc card writing 
# --------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm-zeromass_ckm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s u~ c~ d~ s~ b b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lep = l+ l- vl vl~
"""+proc+"""
output -f""")
fcard.close()

# --------------------------------------------------------------
#  Additional run card options
# --------------------------------------------------------------
run_card_extras = { 'lhaid'         : lhaid,
                    'pdlabel'       : "'"+pdflabel+"'",
                    'parton_shower' : parton_shower,
                    'maxjetflavor'  : maxjetflavor,
                    'reweight_scale': reweight_scale,
                    'reweight_PDF'  : reweight_PDF,
                    'PDF_set_min'   : pdfmin,
                    'PDF_set_max'   : pdfmax,
                    'muR_over_ref'  : muR_over_ref,
                    'muF1_over_ref' : muF1_over_ref,
                    'muF2_over_ref' : muF2_over_ref,
                    'dynamical_scale_choice' : dyn_scale,
                    'jetalgo'   : '-1',  # use anti-kT jet algorithm
                    'jetradius' : '0.4', # set jet cone size of 0.4
                    'ptj'       : '0.1', # minimum jet pT
                    'req_acc'   : '0.001',
                    }


# --------------------------------------------------------------
#  Check the beam energy
# --------------------------------------------------------------
            
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


# Decay with MadSpin
madspin_card_loc='madspin_card.dat'
madspin_card_rep = madspin_card_loc
madspin_in       = 'import Events/'+runName+'/events.lhe'
madspin_rep      = 'set ms_dir MadSpin'
madspin_seed     = runArgs.randomSeed
if hasattr(runArgs, 'inputGenConfFile'):
    madspin_card_rep = gridpack_dir+'Cards/'+madspin_card_loc
    madspin_in       = 'import '+gridpack_dir+'Events/'+runName+'/events.lhe'
    madspin_rep      = 'set ms_dir '+gridpack_dir+'MadSpin'
    madspin_seed     = 10000000+int(runArgs.randomSeed)

mscard = open(madspin_card_rep,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event
set Nevents_for_max_weigth 500
set BW_cut %i
set seed %i
%s
%s
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lep = l+ l- vl vl~
\n
"""%(bwcut,madspin_seed,madspin_in,madspin_rep))

mscard.write("""%s\nlaunch"""%(wdecay))
mscard.close()

# --------------------------------------------------------------
#  Start building the cards
# --------------------------------------------------------------
process_dir = new_process(grid_pack=gridpack_dir)

# Param card
paramNameToCopy      = 'MadGraph_param_card_ttbar_Wcb_aMcNLO.dat'
paramNameDestination = 'param_card.dat'
paramcard            = subprocess.Popen(['get_files','-jo',paramNameToCopy]).communicate()
if not os.access(paramNameToCopy,os.R_OK):
    raise RuntimeError("ERROR: Could not get %s"%(paramNameToCopy))

build_param_card(param_card_old=paramNameToCopy,param_card_new=paramNameDestination)

shutil.copyfile(paramNameDestination,process_dir+'/Cards/'+paramNameDestination)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
	  run_card_new='run_card.dat',
	  nevts=nevents,
	  rand_seed=runArgs.randomSeed,
	  beamEnergy=beamEnergy,
	  xqcut=0.,
	  extras=run_card_extras
	  )

#Print param card 
print_cards()


fileN = process_dir+'/SubProcesses/setscales.f'
mark  = '      elseif(dynamical_scale_choice.eq.10) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 0                                    cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'write(*,*) "User-defined scale not set"',
           'stop 1',
           'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           'tmp = 0',
           'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc'
           ]

for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
           toKeep = False
           break
    if toKeep:
        print line,
    if line.startswith(mark):
        print """
c         Q^2= mt^2 + 0.5*(pt^2+ptbar^2)
          xm2=dot(pp(0,3),pp(0,3))
          tmp=sqrt(xm2+0.5*(pt(pp(0,3))**2+pt(pp(0,4))**2))
          temp_scale_id='mt**2 + 0.5*(pt**2+ptbar**2)'
              """



generate(run_card_loc='run_card.dat',
		 param_card_loc=None,
		 madspin_card_loc=madspin_card_loc,
		 mode=mode,
		 njobs=1,
		 proc_dir=process_dir,
		 run_name=runName,
		 grid_pack=gridpack_mode,
		 gridpack_dir=gridpack_dir,
		 nevents=nevents,
		 random_seed=runArgs.randomSeed,
		 required_accuracy=0.001
		)

outputDS = arrange_output(run_name=runName,
				   proc_dir=process_dir,
				   outputDS=runName+'._00001.events.tar.gz',
				   lhe_version=lhe_version)                   
